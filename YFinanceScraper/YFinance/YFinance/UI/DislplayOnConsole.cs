﻿using System;
using System.Runtime.InteropServices;
using YFinance.Containers;

namespace YFinance
{
    public static class DislplayOnConsole
    {
        public static void Display(PortfolioState state)
        {
            Console.WriteLine("Overal portfolio health at {0}", DateTime.Today);
            Console.WriteLine($"Current Market Value: ${state.CurrentMarketValue}");
            DescripeThenPrintGainInColor( state.DayGainUSD, "Day Gain $");
            Console.Write(" ");
            DescripeThenPrintGainInColor(state.DayGainPercent);
            Console.WriteLine("%");
            DescripeThenPrintGainInColor( state.TotalGainUSD, "Total Gain $");
            Console.Write(" ");
            DescripeThenPrintGainInColor(state.TotalGainPercent);
            Console.WriteLine("%");
            Console.WriteLine("*****************************");

            foreach (var stock in state.StocksState)
            {
                
                Console.Write(stock.Symbol+" ");
                Console.Write("$");
                Console.Write(stock.CurrentPrice);
                Console.Write(" $");
                DescripeThenPrintGainInColor(stock.PriceChangeDollars);
                Console.Write(" ");
                DescripeThenPrintGainInColor(stock.PriceChangePercent);
                Console.WriteLine("%");
                Console.WriteLine("-----------------------");
            }
            Console.WriteLine("Done.");
        }

        private static void PrintInGreenOrRed(double value)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            if (value < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }


            Console.Write(value);
            Console.ResetColor();
        }

        private static void DescripeThenPrintGainInColor( double gain, string description = "")
        {
            Console.Write(description);
            PrintInGreenOrRed(gain);
        }

    }
}