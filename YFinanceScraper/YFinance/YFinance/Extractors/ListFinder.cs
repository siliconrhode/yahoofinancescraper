﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace YFinance
{
    public static class ListFinder

    {
        public static IWebElement LoadPortfolio(IWebDriver driver, string portfolioName)

        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(90));

            if (portfolioName == "Retirement")
            {
                driver.Navigate().GoToUrl("https://finance.yahoo.com/portfolio/p_0/view/v1");

                try
                {
                    wait.Until(drv => drv.FindElement(By.Id("main")));
                }
                catch
                {
                    Console.WriteLine("Failed to load Retirement Portfolio");
                }

                Console.WriteLine("Retirement Portfolio loaded successfully");
                Console.WriteLine();
                

            }

            IWebElement portfolio = driver.FindElement(By.Id("main"));
            return portfolio;

        }

    }
}
