﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using YFinance.Containers;

namespace YFinance.Extractors
{
    public static class PortfolioExtractor
    {
        public static PortfolioState Extract(IWebElement portfolio, IWebDriver driver)
        {
            DateTime currentTime = DateTime.Now;
            var portfolioStatusNow = new PortfolioState();

            //Regex pattern finder
            var extractUsdRegex =  new Regex("^[+-]?[\\S]+");
            var extractPercentRegex = new Regex("[+-]?\\d+\\.?\\d+(?=%)");
            //xPath locators
            var currentMarketValueHtml = portfolio.FindElement(By.ClassName("_3wreg")).Text;
            var dayGainHtml = portfolio.FindElement(By.XPath("//*[contains(text(),'Day Gain')]/span")).Text;
            var totalGainHtml = portfolio.FindElement(By.XPath("//*[contains(text(),'Total Gain')]/span")).Text;
            //extracted values as strings
            var dayGainUSD = extractUsdRegex.Match(dayGainHtml).ToString();
            var dayGainPercent = extractPercentRegex.Match(dayGainHtml).ToString();
            var totalGainUSD = extractUsdRegex.Match(totalGainHtml).ToString();
            var totalGainPercent = extractPercentRegex.Match(totalGainHtml).ToString();
            //Store extracted values as double in Portfolio status Now
            portfolioStatusNow.TimeOfExtraction = currentTime;
            portfolioStatusNow.CurrentMarketValue = double.Parse(currentMarketValueHtml, NumberStyles.Currency);
            portfolioStatusNow.DayGainUSD = double.Parse(dayGainUSD, NumberStyles.Currency);
            portfolioStatusNow.DayGainPercent = double.Parse(dayGainPercent);
            portfolioStatusNow.TotalGainUSD = double.Parse(totalGainUSD, NumberStyles.Currency);
            portfolioStatusNow.TotalGainPercent = double.Parse(totalGainPercent);
            //find all tables in the current page
            IList<IWebElement> table = portfolio.FindElements(By.TagName("table"));
            //store the stocks table
            var stocksStateWebElement = table[1];
            //store a snap shot of stocks state at time of extraction
            List<StockState> allMyStocksCurrentStateList = StateExtractor.Extract(stocksStateWebElement, driver);




            portfolioStatusNow.StocksState = allMyStocksCurrentStateList;
           
            

            return portfolioStatusNow;
        }
    }
}