﻿using System.Data.Entity;
using YFinance.Containers;

namespace YFinance
{
    public class PortfolioContext : DbContext
    {
        public PortfolioContext()
            : base("PortfolioDatabase")
        {

        }
        public DbSet<PortfolioState> PortfolioState { get; set; }
        //public DbSet<StockState> StockState { get; set; }
    }
}