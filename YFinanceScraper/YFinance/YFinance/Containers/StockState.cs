﻿namespace YFinance.Containers
{
    public class StockState
    {
        public int Id { get; set; }
        public string Symbol { get; set; }
        public double CurrentPrice { get; set; }
        public double PriceChangeDollars { get; set; }
        public double PriceChangePercent { get; set; }
      
        public virtual PortfolioState PortfolioState { get; set; }
    }
}