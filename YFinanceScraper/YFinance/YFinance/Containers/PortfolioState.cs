﻿using System;
using System.Collections.Generic;

namespace YFinance.Containers
{
    public class PortfolioState
    {
        public int Id { get; set; }
        public DateTime TimeOfExtraction { get; set; }
        public double CurrentMarketValue { get; set; }
        public double DayGainUSD { get; set; }
        public double DayGainPercent { get; set; }
        public double TotalGainUSD { get; set; }
        public double TotalGainPercent { get; set; }
        public virtual List<StockState> StocksState { get; set; }
    }
}