﻿using System;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;

namespace YFinance
{
    public static class StateExtractor
    {
        public static List<StockState> Extract(IWebElement positionData)
        {
            var stocksStateList = new List<StockState>();
            var tableRows = positionData.FindElements(By.XPath("//tbody"));

            using (var progress = new ProgressBar())

            {
                for (int i = 1, j = tableRows.Count; i <= j; i++)
                
                    var thisRow = tableRows[i - 1];

                    var stockState = new StockState();

                    var symbolPriceArr = thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td/span/span")).Text
                        .Split(new[] {"\r\n"}, StringSplitOptions.None);
                    stockState.Symbol = symbolPriceArr[0];
                    stockState.CurrentPrice = double.Parse(symbolPriceArr[1]);

                    stockState.PriceChangeDollars =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[2]/span[2]")).Text);
                    stockState.PriceChangePercent =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[2]/span[1]")).Text.Trim('%'));
                    stockState.Shares = double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[3]")).Text);
                    stockState.CostBasis = double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[4]")).Text,
                        NumberStyles.Any);
                    stockState.MarketValue =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[5]/span")).Text,
                            NumberStyles.Any);
                    stockState.DayGainPercent =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[6]/span[1]")).Text.Trim('%'),
                            NumberStyles.Any);
                    stockState.DayGainDollars =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[6]/span[2]")).Text,
                            NumberStyles.Any);
                    stockState.TotalGainDollars =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[7]/span[2]")).Text,
                            NumberStyles.Any);
                    stockState.TotalGainPercent =
                        double.Parse(thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[7]/span[1]")).Text.Trim('%'),
                            NumberStyles.Any);
                    stockState.LotCount =
                        double.Parse(
                            thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[8]")).Text
                                .Split(new[] {" "}, StringSplitOptions.None)[0], NumberStyles.Any);
                    stockState.Notes = thisRow.FindElement(By.XPath($"//tbody[{i}]/tr/td[9]")).Text;



                    stocksStateList.Add(stockState);

                    progress.Report((double) i / j);





                }

                return stocksStateList;

            }
        }
}
