﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using YFinance.Containers;
using YFinance.Extractors;

namespace YFinance
{
    class Program
    {
        static void Main(string[] args)
        {
           
            var currentGeneralState = new PortfolioState();

            // Instantiate ChromeDriver and sign in to Yahoo finance
            IWebDriver driver = new ChromeDriver();
            Authenticator.SignIn(driver);
            IWebElement portfolio = ListFinder.LoadPortfolio(driver, "Retirement");
            currentGeneralState = PortfolioExtractor.Extract(portfolio, driver);
            DislplayOnConsole.Display(currentGeneralState);
            
            try
            {
                using (var db = new PortfolioContext())
                {
                    db.PortfolioState.Add(currentGeneralState);
                    db.SaveChanges();
                    Console.WriteLine("Sucessfully Saved Portfolio Data");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to save to database " + ex);
            }

        }
    }
}
