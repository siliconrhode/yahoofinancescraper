﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using YFinance.UI;

namespace YFinance
{
    public static class Authenticator
    {
        public static void SignIn(IWebDriver driver)
        {
            using (var progress = new ProgressBar())
            {
                Console.Clear();
                Console.WriteLine("Signing in Please wait...");
                progress.Report((double)0 / 4);

                var url =
                    "https://login.yahoo.com/?.intl=us&.lang=en-US&.src=finance&authMechanism=primary&yid=&done=https%3A%2F%2Ffinance.yahoo.com%2F&eid=100&add=1";
                driver.Navigate().GoToUrl(url);

                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(90));

                //Grab login in box store it ans type user name
                try
                {
                    wait.Until(drv => drv.FindElement(By.Id("login-username")));

                }
                catch 

                {
                    Console.WriteLine("Unable to access Yahoo finance Please check your Internet connection");
                }
                progress.Report((double)1 / 4);

                IWebElement loginBox = driver.FindElement(By.Id("login-username"));

                loginBox.SendKeys("Samsoffar");
                loginBox.Submit();

                try
                {
                    wait.Until(d => d.FindElement(By.Id("login-passwd")));
                }
                catch
                {
                    throw new Exception("Invalid Username!");
                }

                progress.Report((double)2 / 4);

                //Password
                IWebElement pwField = driver.FindElement(By.Id("login-passwd"));
                pwField.SendKeys("Project123");

                //Click the sign in Button
                IWebElement signInButton = driver.FindElement(By.Id("login-signin"));
                signInButton.Click();
                progress.Report((double)4 / 4);


                try
                {
                    wait.Until(drv => drv.Url.StartsWith("https://finance.yahoo.com"));
                }
                catch
                {
                    Console.WriteLine("Invalid User Name / combination");
                }
                Console.WriteLine();
                Console.WriteLine("signed in successfully ");
                
            }
        }
    }
}