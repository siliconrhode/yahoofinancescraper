// <auto-generated />
namespace YFinance.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddedIdFieldsToEntities : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedIdFieldsToEntities));
        
        string IMigrationMetadata.Id
        {
            get { return "201803270014265_AddedIdFieldsToEntities"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
