namespace YFinance.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIdFieldsToEntities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StockStates", "PortfolioStatus_Id", "dbo.PortfolioStatus");
            DropIndex("dbo.StockStates", new[] { "PortfolioStatus_Id" });
            CreateTable(
                "dbo.PortfolioStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeOfExtraction = c.DateTime(nullable: false),
                        CurrentMarketValue = c.Double(nullable: false),
                        DayGainUSD = c.Double(nullable: false),
                        DayGainPercent = c.Double(nullable: false),
                        TotalGainUSD = c.Double(nullable: false),
                        TotalGainPercent = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.StockStates", "PortfolioState_Id", c => c.Int());
            CreateIndex("dbo.StockStates", "PortfolioState_Id");
            AddForeignKey("dbo.StockStates", "PortfolioState_Id", "dbo.PortfolioStates", "Id");
            DropColumn("dbo.StockStates", "Shares");
            DropColumn("dbo.StockStates", "CostBasis");
            DropColumn("dbo.StockStates", "MarketValue");
            DropColumn("dbo.StockStates", "DayGainDollars");
            DropColumn("dbo.StockStates", "DayGainPercent");
            DropColumn("dbo.StockStates", "TotalGainDollars");
            DropColumn("dbo.StockStates", "TotalGainPercent");
            DropColumn("dbo.StockStates", "LotCount");
            DropColumn("dbo.StockStates", "Notes");
            DropColumn("dbo.StockStates", "PortfolioStatus_Id");
            DropTable("dbo.PortfolioStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PortfolioStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeOfExtraction = c.DateTime(nullable: false),
                        CurrentMarketValue = c.Double(nullable: false),
                        NetDayGainDollars = c.Double(nullable: false),
                        NetDayGainPercent = c.Double(nullable: false),
                        NetTotalGainDollars = c.Double(nullable: false),
                        NetTotalGainPercent = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.StockStates", "PortfolioStatus_Id", c => c.Int());
            AddColumn("dbo.StockStates", "Notes", c => c.String());
            AddColumn("dbo.StockStates", "LotCount", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "TotalGainPercent", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "TotalGainDollars", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "DayGainPercent", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "DayGainDollars", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "MarketValue", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "CostBasis", c => c.Double(nullable: false));
            AddColumn("dbo.StockStates", "Shares", c => c.Double(nullable: false));
            DropForeignKey("dbo.StockStates", "PortfolioState_Id", "dbo.PortfolioStates");
            DropIndex("dbo.StockStates", new[] { "PortfolioState_Id" });
            DropColumn("dbo.StockStates", "PortfolioState_Id");
            DropTable("dbo.PortfolioStates");
            CreateIndex("dbo.StockStates", "PortfolioStatus_Id");
            AddForeignKey("dbo.StockStates", "PortfolioStatus_Id", "dbo.PortfolioStatus", "Id");
        }
    }
}
