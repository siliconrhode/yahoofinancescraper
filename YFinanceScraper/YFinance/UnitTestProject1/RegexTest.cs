﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RegexTest()
        {
            //arrange
            var expectedPercent = "-98.36";
            var expectedUsd = "-6,180,018.00";
            var sourcestring = "-6,180,018.00 (-98.36%)";
            var extractUsdRegex = new Regex("^[+-]?[\\S]+");
            var extractPercentRegex = new Regex("[+-]?\\d+\\.?\\d+(?=%)");
            // Act
            var actualPercent = extractPercentRegex.Match(sourcestring).ToString();
            //Assert 
            Assert.AreEqual(expectedPercent,actualPercent);
            
        }
    }
    
}
